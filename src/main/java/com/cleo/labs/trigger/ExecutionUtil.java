package com.cleo.labs.trigger;

import com.cleo.lexicom.beans.LexActionBean;
import com.cleo.lexicom.external.IActionController;
import com.cleo.lexicom.external.ILexiCom;

import java.util.Arrays;

/**
 * Created by sbelur on 20/09/15.
 */
public class ExecutionUtil {

    public static void execute(String[] commands,String[] actionAliasPath,ILexiCom lexicom) throws Exception {
        if(lexicom == null){
            Logger.debug("Lexicom instance is null");
            return;
        }

        final IActionController actionController = lexicom.getActionController(actionAliasPath);

        for (String command : commands) {
            for (String subcommand : command.split("[\r\n]+")) {
                Logger.debug("running " + subcommand);
                try {
                    actionController.execute(subcommand);
                } catch (Exception e) {
                    Logger.debug(e.toString());
                }
            }
        }
        try {
            ((LexActionBean) actionController).APIrunRemoteIndex = 0;
            actionController.end();
        } catch (Exception e) {
            Logger.debug("exception running action " + Arrays.toString(actionAliasPath));
            Logger.debug(e);
        } finally {
            if (Configuration.getConfiguration().getRemove()) {
                try {
                    lexicom.remove(ILexiCom.ACTION, actionAliasPath);
                } catch (Exception f) {
                    Logger.debug("exception removing action " + Arrays.toString(actionAliasPath));
                    Logger.debug(f);
                }
            } else {
                Logger.debug("action removal disabled");
            }
        }
    }


}
