package com.cleo.labs.trigger.cluster.support;

/**
 * Created by sbelur on 22/09/15.
 */
public enum ClusterMode {
    ON,
    OFF;

    @Override
    public String toString() {
        return super.toString().toLowerCase();
    }
}
