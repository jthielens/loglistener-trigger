package com.cleo.labs.trigger.cluster.support;

import akka.actor.*;
import akka.cluster.Cluster;
import akka.contrib.pattern.*;
import com.cleo.labs.trigger.Logger;
import com.cleo.labs.trigger.persist.akka.messages.ActionCommand;
import com.cleo.labs.trigger.persist.akka.persistor.LexEventPersistor;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import scala.collection.immutable.Seq;

import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Created by sbelur on 22/09/15.
 */
public class ClusterSystem {

    private static final ClusterSystem INSTANCE = new ClusterSystem();

    private AtomicBoolean configured = new AtomicBoolean(false);

    private ActorRef clusterProxy;

    private ActorSystem system;

    private ClusterSystem(){}

    public static ClusterSystem getInstance(){return INSTANCE;}

    /***
     * <pre>
     *
     * a. Reads cluster config
     * b. Starts the singleton manager (For Persistence only)
     * c. Starts the singleton proxy
     *
     * </pre>
     */
    public void initalize(){
        if(configured.compareAndSet(false,true)){
            Config rootConfig = ConfigFactory.load();
            system = ActorSystem.create("event-processing-cluster", rootConfig);
            system.actorOf(ClusterSingletonManager.defaultProps(
                    Props.create(LexEventPersistor.class, ClusterMode.ON), "lexEventPersistor",
                    // "" -> No role indication - Here we are not concerned with roles. All entities comes up in a VM.
                    PoisonPill.getInstance(), ""), "singleton");

            clusterProxy = system.actorOf(ClusterSingletonProxy.defaultProps("/user/singleton/lexEventPersistor",
                    // "" -> No role indication - Here we are not concerned with roles. All entities comes up in a VM.
                    ""), "lexEventPersistorProxy");
        }
        else {
            Logger.debug("Cluster system already configured");
        }


    }


    public ActorRef getClusterProxy() {
        return clusterProxy;
    }


    public ActorSystem getActorSystem() {
        return system;
    }
}
