package com.cleo.labs.trigger.persist.akka.events;

import java.io.Serializable;

/**
 * Created by sbelur on 20/09/15.
 */
public final class ConfirmationLexEvent implements Serializable{

    private static final long serialVersionUID = 1L;

    private final Long eventDeliveryId;

    public ConfirmationLexEvent(Long eventDeliveryId) {
        this.eventDeliveryId = eventDeliveryId;
    }


    public Long getEventDeliveryId() {
        return eventDeliveryId;
    }
}
