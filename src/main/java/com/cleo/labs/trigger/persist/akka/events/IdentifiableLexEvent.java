package com.cleo.labs.trigger.persist.akka.events;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

/**
 * Created by sbelur on 20/09/15.
 */
public final class IdentifiableLexEvent implements Serializable{

    private static final long serialVersionUID = 1L;

    private final Long eventDeliveryId;
    private final LexEvent lexEvent;

    public IdentifiableLexEvent(Long eventDeliveryId, LexEvent lexEvent) {
        this.eventDeliveryId = eventDeliveryId;
        this.lexEvent = lexEvent;
    }

    public Long getEventDeliveryId() {
        return eventDeliveryId;
    }

    public LexEvent getLexEvent() {
        return lexEvent;
    }



    public long getLatency(){
        long end = System.currentTimeMillis();
        List list = Arrays.asList(lexEvent.getParameters());
        int index = list.indexOf("startTime");
        if(index != -1)
            return (end - Long.parseLong(lexEvent.getParameters()[index+1]));
        return -1;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        IdentifiableLexEvent that = (IdentifiableLexEvent) o;

        return !(eventDeliveryId != null ? !eventDeliveryId.equals(that.eventDeliveryId) : that.eventDeliveryId != null);

    }

    @Override
    public int hashCode() {
        return eventDeliveryId != null ? eventDeliveryId.hashCode() : 0;
    }


    @Override
    public String toString() {
        return "IdentifiableLexEvent{" +
                "eventDeliveryId=" + eventDeliveryId +
                ", lexEvent=" + lexEvent +
                '}';
    }
}
