package com.cleo.labs.trigger.persist.akka.events;

import java.io.Serializable;
import java.util.Arrays;

/**
 * Created by sbelur on 20/09/15.
 */
public final class LexEvent implements Serializable{


    private static final long serialVersionUID = 1L;

    private final String[] commandsToExecute;
    private final String[] actionAliasPath;
    private final String eventName;
    private final String[] parameters;
    private final String sessionid;

    public LexEvent(String[] commandsToExecute, String[] actionAliasPath, String eventName, String[] parameters, String sessionid) {
        this.commandsToExecute = commandsToExecute;
        this.actionAliasPath = actionAliasPath;
        this.eventName = eventName;
        this.parameters = parameters;
        this.sessionid = sessionid;
    }

    public String[] getCommandsToExecute() {
        return commandsToExecute;
    }

    public String[] getActionAliasPath() {
        return actionAliasPath;
    }

    public String getEventName() {
        return eventName;
    }

    public String[] getParameters() {
        return parameters;
    }


    public String getSessionid() {
        return sessionid;
    }

    @Override
    public String toString() {
        return "LexEvent{" +
                "commandsToExecute=" + Arrays.toString(commandsToExecute) +
                ", actionAliasPath=" + Arrays.toString(actionAliasPath) +
                ", eventName='" + eventName + '\'' +
                ", parameters=" + Arrays.toString(parameters) +
                '}';
    }
}
