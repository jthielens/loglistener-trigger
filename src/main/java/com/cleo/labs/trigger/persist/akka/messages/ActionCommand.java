package com.cleo.labs.trigger.persist.akka.messages;

import scala.Tuple2;

import java.io.Serializable;

/**
 * Created by sbelur on 20/09/15.
 */
public class ActionCommand  implements Serializable{

    private static final long serialVersionUID = 1L;
    private final String[] commands;
    private final String[] aliasPath;
    private final String eventName;
    private final String[] parameters;
    private final String sessionid;


    public ActionCommand(String[] commands, String[] actionAliasPath, String eventName, String[] parameters, String sessionid) {
        this.commands = commands;
        this.aliasPath = actionAliasPath;
        this.eventName = eventName;
        this.parameters = parameters;
        this.sessionid = sessionid;
    }

    public String[] commands(){
        return commands;
    }
    public String[] aliasPath(){
        return aliasPath;
    }


    public String getEventName() {
        return eventName;
    }

    public String[] getParameters() {
        return parameters;
    }

    public String getSessionid() {
        return sessionid;
    }
}


