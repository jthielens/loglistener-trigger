package com.cleo.labs.trigger.persist.akka.messages;

import java.io.Serializable;

/**
 * Created by sbelur on 20/09/15.
 */
public class ConfirmEventExecution implements Serializable{

    private static final long serialVersionUID = 1L;


    private final Long deliveryId;

    public ConfirmEventExecution(Long deliveryId) {
        this.deliveryId = deliveryId;
    }

    public Long getDeliveryId() {
        return deliveryId;
    }
}
