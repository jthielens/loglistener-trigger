package com.cleo.labs.trigger.persist.akka.messages;

/**
 * Created by sbelur on 01/10/15.
 */
public final class InitaiteLogout {
    private final String sessionid;

    public InitaiteLogout(String sessionid) {
        this.sessionid = sessionid;
    }

    public String getSessionid() {
        return sessionid;
    }
}
