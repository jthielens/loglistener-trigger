package com.cleo.labs.trigger.persist.akka.persistor;

import akka.actor.ActorPath;
import akka.actor.ActorRef;
import akka.actor.Props;
import akka.cluster.routing.ClusterRouterConfig;
import akka.cluster.routing.ClusterRouterPool;
import akka.cluster.routing.ClusterRouterPoolSettings;
import akka.cluster.routing.ClusterRouterSettings;
import akka.event.Logging;
import akka.event.LoggingAdapter;
import akka.japi.Function;
import akka.japi.Procedure;
import akka.persistence.RecoveryCompleted;
import akka.persistence.UntypedPersistentActorWithAtLeastOnceDelivery;
import akka.routing.*;
import com.cleo.labs.trigger.cluster.support.ClusterMode;
import com.cleo.labs.trigger.persist.akka.events.ConfirmationLexEvent;
import com.cleo.labs.trigger.persist.akka.messages.ActionCommand;
import com.cleo.labs.trigger.persist.akka.messages.ConfirmEventExecution;
import com.cleo.labs.trigger.persist.akka.events.IdentifiableLexEvent;
import com.cleo.labs.trigger.persist.akka.events.LexEvent;
import com.cleo.labs.trigger.persist.akka.worker.EventExecutor;
import com.cleo.labs.trigger.persist.akka.worker.SessionEventExecutor;
import scala.concurrent.duration.FiniteDuration;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Created by sbelur on 20/09/15.
 * <pre>
 *   Supports atleast once delivery guarantee mechanism by persisting events
 *
 *   At a high level, persists events for the command received. Once persisted,
 *   the event is dispatched to a worker actor. Delivery is deemed confirmed,
 *   when the worker actor confirms the same delivery Id sent to it.
 *
 *   If the worker crashes after persistence, replay is triggered when this
 *   Actor (Persistor) recovers back.
 *
 * </pre>
 */
public class LexEventPersistor extends UntypedPersistentActorWithAtLeastOnceDelivery {

    private final ClusterMode clusterMode;
    private ActorRef routerRef;
    private ActorRef sessionRef;

    private LoggingAdapter log = Logging.getLogger(getContext().system(), this);


    public LexEventPersistor(ClusterMode clusterMode) {
        this.clusterMode = clusterMode;
    }


    @Override
    public void preStart() throws Exception {
        super.preStart();

        //TODO - externalize this

        if (clusterMode == ClusterMode.OFF) {
            initRoutersForLocalMode();

        } else {
            initRoutersForClusterAwareness();
            //TODO build session ref for cluster....
        }


    }

    private void initRoutersForClusterAwareness() {

        // TODO: Externalize config in .conf file.
        // TODO : Do we merge these routers ?

        int totalInstances = 500;
        int maxInstancesPerNode = 250;
        boolean allowLocalRoutees = true;
        routerRef = getContext().actorOf(
                new ClusterRouterPool(new RoundRobinPool(0),
                        new ClusterRouterPoolSettings(totalInstances, maxInstancesPerNode,
                                allowLocalRoutees, "")).props(Props
                        .create(EventExecutor.class)), "clusterworkerRouter");


        sessionRef = getContext().actorOf(
                new ClusterRouterPool(new ConsistentHashingPool(0).withHashMapper(new ConsistentHashingRouter.ConsistentHashMapper() {

                    @Override
                    public Object hashKey(Object message) {
                        if (message instanceof IdentifiableLexEvent) {
                            String hashingId = ((IdentifiableLexEvent) message).getLexEvent().getSessionid();
                            log.info("Dispatching to cluster worker for hash id " + hashingId);
                            return hashingId;
                        }
                        return null;
                    }
                }),
                        new ClusterRouterPoolSettings(totalInstances, maxInstancesPerNode,
                                allowLocalRoutees, "")).props(Props
                        .create(SessionEventExecutor.class)), "clustersessionexecutorrouter");
    }

    private void initRoutersForLocalMode() {
        routerRef = getContext().actorOf(new SmallestMailboxPool(500).props(Props.create(EventExecutor.class)),
                "eventExecutorRouter");

        sessionRef = getContext().actorOf(new ConsistentHashingPool(500).withHashMapper(new ConsistentHashingRouter.ConsistentHashMapper() {

            @Override
            public Object hashKey(Object message) {
                if (message instanceof IdentifiableLexEvent) {
                    String hashingId = ((IdentifiableLexEvent) message).getLexEvent().getSessionid();
                    log.info("Dispatching for hash id " + hashingId);
                    return hashingId;
                }
                return null;
            }
        }).props(Props.create(SessionEventExecutor.class)), "sessionexecutorrouter");
    }

    @Override
    public void onReceiveRecover(Object event) throws Exception {
        if (!(event instanceof RecoveryCompleted)) {
            log.info("In recover call for event " + event);
            handleEvent(event);
        }
    }

    // Temporary - Just to ensure that crash test passes.
    @Override
    public FiniteDuration redeliverInterval() {
        return new FiniteDuration(60, TimeUnit.SECONDS);
    }

    @Override
    public void onReceiveCommand(Object message) throws Exception {
        if (message instanceof ActionCommand) {
            ActionCommand actionCommand = (ActionCommand) message;
            persist(new LexEvent(actionCommand.commands(), actionCommand.aliasPath(), actionCommand.getEventName(), actionCommand.getParameters(), actionCommand.getSessionid()), new Procedure<LexEvent>() {
                public void apply(LexEvent evt) {
                    onEventPersisted(evt);
                }
            });
        } else if (message instanceof ConfirmEventExecution) {
            ConfirmEventExecution confirmMsg = (ConfirmEventExecution) message;
            persist(new ConfirmationLexEvent(confirmMsg.getDeliveryId()), new Procedure<ConfirmationLexEvent>() {
                public void apply(ConfirmationLexEvent evt) {
                    onEventPersisted(evt);
                }
            });
        } else {
            unhandled(message);
        }
    }

    @Override
    public String persistenceId() {
        String pId = getSelf().path().parent().name() + "-" + getSelf().path().name();
        log.info("Persistent Id " + pId);
        System.out.println("Persistent Id " + pId);
        return pId;
    }


    private void onEventPersisted(final Object event) {
        handleEvent(event);
    }

    private void handleEvent(Object event) {
        if (event instanceof LexEvent) {
            ActorPath destination = routerRef.path();
            final LexEvent evt = (LexEvent) event;
            List<String> params = Arrays.asList(evt.getParameters());
            int mbIndex = params.indexOf("mailbox");
            if (mbIndex != -1) {
                String mb = params.get(mbIndex + 1);
                //TEMPORARY means to identify session based handling for a user
                // Right now all events for this user will be batched.
                if ("batch_ftp".equals(mb)) {
                    destination = sessionRef.path();
                }
            }
            deliver(destination, new Function<Long, Object>() {

                @Override
                public Object apply(Long deliveryId) throws Exception {
                    return new IdentifiableLexEvent(deliveryId, evt);
                }
            });

        } else if (event instanceof ConfirmationLexEvent) {
            final ConfirmationLexEvent evt = (ConfirmationLexEvent) event;
            confirmDelivery(evt.getEventDeliveryId());
        }
    }


}
