package com.cleo.labs.trigger.persist.akka.worker;

import akka.actor.Actor;
import akka.actor.ActorLogging;
import akka.actor.UntypedActor;
import akka.event.Logging;
import akka.event.LoggingAdapter;
import com.cleo.labs.trigger.ExecutionUtil;
import com.cleo.labs.trigger.persist.akka.events.IdentifiableLexEvent;
import com.cleo.labs.trigger.persist.akka.events.LexEvent;
import com.cleo.labs.trigger.persist.akka.messages.ConfirmEventExecution;
import com.cleo.lexicom.external.ILexiCom;
import com.cleo.lexicom.external.LexiComFactory;
import scala.PartialFunction;
import scala.runtime.BoxedUnit;

import java.util.Arrays;
import java.util.List;

/**
 * Created by sbelur on 20/09/15.
 * <pre>
 * Worker which executes commands of the specified action.
 * If successful, sends back a confirmation for the same delivery Id to event persistor.
 * </pre>
 */
public class EventExecutor extends UntypedActor {

    private LoggingAdapter log = Logging.getLogger(getContext().system(), this);

    @Override
    public void preStart(){
       log.info("Created worker actor " + getSelf().path());	
    }

    @Override
    public void onReceive(Object message) throws Exception {
        if (message instanceof IdentifiableLexEvent) {
            IdentifiableLexEvent msg = (IdentifiableLexEvent) message;

            long latency = msg.getLatency();
            List<String> alias = Arrays.asList(msg.getLexEvent().getActionAliasPath());
            List<String> params = Arrays.asList(msg.getLexEvent().getParameters());
            if(latency != -1) // File events only
                log.info("File Event Latency found to be "+ latency + " for parameters "+params);

            // A temporary mechanism to show case failure handling
            // Just a sleep so that second attempt can be made successful.
            if(params.toString().contains("sleep.txt")){
                log.info("Intentionally sleeping for 10 secs now if filename is sleep.txt to check recovery - try crashing before sleep is done");
                Thread.sleep(10000);
                log.info("Sleep over");
            }
            try {
                ExecutionUtil.execute(msg.getLexEvent().getCommandsToExecute(), msg.getLexEvent().getActionAliasPath(), lexicom());
                log.info("Executed command "+msg.getLexEvent().getCommandsToExecute());
            }
            catch (Exception e){
                // TEMPORARY - need to find the root cause of Action Already running error
                log.error("Error while executing command "+e.getMessage());
            }
            getSender().tell(new ConfirmEventExecution(msg.getEventDeliveryId()), getSelf());
        } else {
            unhandled(message);
        }
    }

    private ILexiCom lexicom() {
        ILexiCom lexicom = null;
        try {
            lexicom = LexiComFactory.getCurrentInstance();
        } catch (Exception ignore) {
            ignore.printStackTrace();
        }
        return lexicom;
    }
}
