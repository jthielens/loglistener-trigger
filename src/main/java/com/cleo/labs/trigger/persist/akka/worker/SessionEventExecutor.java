package com.cleo.labs.trigger.persist.akka.worker;

import akka.actor.ActorRef;
import akka.actor.UntypedActor;
import akka.event.Logging;
import akka.event.LoggingAdapter;
import com.cleo.labs.trigger.Configuration;
import com.cleo.labs.trigger.ExecutionUtil;
import com.cleo.labs.trigger.persist.akka.events.IdentifiableLexEvent;
import com.cleo.labs.trigger.persist.akka.messages.ConfirmEventExecution;
import com.cleo.labs.trigger.persist.akka.messages.InitaiteLogout;
import com.cleo.labs.trigger.session.SessionEvent;
import com.cleo.lexicom.external.ILexiCom;
import com.cleo.lexicom.external.LexiComFactory;
import scala.concurrent.duration.FiniteDuration;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * Created by sbelur on 20/09/15.
 * <pre>
 * worker which groups events for a session in memory as of now till it sees a logout action.
 * This is very naive implementation for POC concept ONLY. Does not take any memory considerations for now.
 *
 * If there is a failure / crash, the persistor will replay all events. So this relies on the reliability of
 * persistor.
 *
 * </pre>
 */
public class SessionEventExecutor extends UntypedActor {

    private LoggingAdapter log = Logging.getLogger(getContext().system(), this);

    // Map(sessionid -> evt container) //TODO : check for optimizing the storage
    private Map<String, SessionEvent> sessionEvents = new HashMap<>();

    private Map<String, Boolean> logoutForcedState = new HashMap<>();

    @Override
    public void onReceive(Object message) throws Exception {
        if (message instanceof IdentifiableLexEvent) {
            IdentifiableLexEvent msg = (IdentifiableLexEvent) message;

            long latency = msg.getLatency();
            List<String> params = Arrays.asList(msg.getLexEvent().getParameters());
            if (latency != -1) // File events only
                log.info("File Event Latency found to be " + latency + " for parameters " + params);


            String sessionid = msg.getLexEvent().getSessionid();
            ActorRef selfActor = getSelf();
            log.info("Received event for session " + sessionid + " in actor " + selfActor + " with path " + selfActor.path());

            // A temporary mechanism to show case failure handling
            // Just a sleep so that second attempt can be made successful.
            if(params.toString().contains("sleep.txt")){
                log.info("Intentionally sleeping for 10 secs now if filename is sleep.txt to check recovery - try crashing before sleep is done");
                Thread.sleep(10000);
                log.info("Sleep over");
            }

            SessionEvent sessionEvent = sessionEvents.get(sessionid);
            if (sessionEvent == null) {
                sessionEvent = new SessionEvent();
                sessionEvents.put(sessionid, sessionEvent);
            }

            if (Configuration.Event.file.name().equals(msg.getLexEvent().getEventName())) {

                if (!logoutForcedState.containsKey(sessionid)) {
                    log.info("Found a file event " + msg);
                    logoutForcedState.put(sessionid, Boolean.TRUE);
                    int forceLogOutAfter = getContext().system().settings().config().getInt("session.force.timeout.in.seconds");
                    ActorRef sender = getSender();
                    getContext().system().scheduler().scheduleOnce(
                            new FiniteDuration(forceLogOutAfter, TimeUnit.SECONDS), selfActor, new InitaiteLogout(sessionid), getContext().system().dispatcher(), sender
                    );
                }

            }

            boolean logout = msg.getLexEvent().getEventName().equals(Configuration.Event.logout.name());
            if (!logout) {
                handleNonLogoutEvent(msg, sessionid, sessionEvent);
            } else {
                handleLogoutEvent(msg, sessionid, sessionEvent);
            }

        } else if (message instanceof InitaiteLogout) {
            InitaiteLogout il = (InitaiteLogout) message;
            String sessionid = il.getSessionid();
            if(logoutForcedState.containsKey(sessionid)) {
                logoutForcedState.remove(sessionid);
                log.info("Handling a forced logout event for session " + sessionid);
                handleLogoutEvent(null, sessionid, sessionEvents.get(sessionid));
            }
        } else {
            unhandled(message);
        }
    }

    private void handleNonLogoutEvent(IdentifiableLexEvent msg, String sessionid, SessionEvent sessionEvent) {
        boolean login = msg.getLexEvent().getEventName().equals(Configuration.Event.login.name());
        if (!login) {
            boolean added = sessionEvent.addToBatch(msg);
            if (added) {
                log.info("Tracking session event for id " + sessionid + " Not executing command for event now - will wait for logout");
            }
        } else {
            executeCommand(msg); // Execute Login command
            getSender().tell(new ConfirmEventExecution(msg.getEventDeliveryId()), getSelf()); // ACK login event
        }
    }

    private void handleLogoutEvent(IdentifiableLexEvent msg, String sessionid, SessionEvent sessionEvent) {
        sessionEvents.remove(sessionid);
        if (sessionEvent != null && sessionEvent.get() != null && !sessionEvent.get().isEmpty()) {
            log.info("Executing now all events for session " + sessionid);
            for (IdentifiableLexEvent bm : sessionEvent.get()) {
                log.info("Executing a event part of session " + sessionid);
                executeCommand(bm);
            }

            // Ack ALL
            for (IdentifiableLexEvent bm : sessionEvent.get()) {
                getSender().tell(new ConfirmEventExecution(bm.getEventDeliveryId()), getSelf());
            }
        }
        // We get a null log out msg in case of timeouts.
        if (msg != null) {
            executeCommand(msg); // Execute Logout command
            getSender().tell(new ConfirmEventExecution(msg.getEventDeliveryId()), getSelf()); // ACK logout event

            // Logout came earlier than scheduled event.
            logoutForcedState.remove(sessionid);
        }
    }

    private void executeCommand(IdentifiableLexEvent bm) {
        try {
            ExecutionUtil.execute(bm.getLexEvent().getCommandsToExecute(), bm.getLexEvent().getActionAliasPath(), lexicom());
        } catch (Exception e) {
            // TEMPORARY - need to find the root cause of Action Already running error
            log.error("Error while executing command " + e.getMessage());
        }
    }

    private ILexiCom lexicom() {
        ILexiCom lexicom = null;
        try {
            lexicom = LexiComFactory.getCurrentInstance();
        } catch (Exception ignore) {
            ignore.printStackTrace();
        }
        return lexicom;
    }
}
