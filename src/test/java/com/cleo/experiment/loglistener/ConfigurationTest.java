package com.cleo.experiment.loglistener;

import static org.junit.Assert.assertEquals;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.yaml.snakeyaml.TypeDescription;
import org.yaml.snakeyaml.Yaml;
import org.yaml.snakeyaml.constructor.Constructor;
import org.yaml.snakeyaml.constructor.SafeConstructor;

import com.cleo.labs.trigger.Configuration;
import com.cleo.labs.trigger.Configuration.Rule;

public class ConfigurationTest {

    private String getLocalResource(String name) {
        try (BufferedInputStream in = new BufferedInputStream(this.getClass().getResourceAsStream(name))) {
            StringBuilder s = new StringBuilder();
            int c;
            while ((c = in.read()) != -1) {
                if (c != '\r') {
                    s.append((char)c);
                }
            }
            return s.toString();
        } catch (IOException e) {
            throw new RuntimeException("resource "+name+" not found");
        }
    }
    private InputStream getLocalResourceStream(String name) {
        return this.getClass().getResourceAsStream(name);
    }

    @Test
    public final void testYaml() {
        String doc = getLocalResource("/test.yaml");
        Yaml yaml = new Yaml();
        Object object =  yaml.load(doc);
        System.out.println(object);
    }

    public final Configuration load(String resource) {
        try {
            Constructor constructor = new Constructor(Configuration.class);
            TypeDescription description = new TypeDescription(Configuration.class);
            description.putListPropertyType("rules", Configuration.Rule.class);
            constructor.addTypeDescription(description);
            Yaml yaml = new Yaml(constructor);
            return yaml.loadAs(getLocalResourceStream(resource), Configuration.class);
        } catch (Exception e) {
            Yaml yaml = new Yaml(new SafeConstructor());
            Object parsed = yaml.load(getLocalResourceStream(resource));
            @SuppressWarnings("unchecked")
            List<Object> list =  (List<Object>)parsed;
            List<Rule> rules = new ArrayList<>();
            for (Object o : list) {
                rules.add(new Rule(o));
            }
            Configuration conf = new Configuration();
            conf.setRules(rules);
            return conf;
        }
    }

    @Test
    public final void testConfiguration() {
        Configuration configuration = load("/test.yaml");
        assertEquals(Configuration.Event.login, configuration.getRules().get(0).event);
        assertEquals(20, configuration.getThreads());
    }

    @Test
    public final void testRulesOnly() {
        Configuration configuration = load("/rulesonly.yaml");
        assertEquals(Configuration.Event.login, configuration.getRules().get(0).event);
        assertEquals(Configuration.DEFAULT_THREADS, configuration.getThreads());
    }
}
